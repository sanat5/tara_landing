import React, { FunctionComponent } from 'react';
import Header from './reusable/Header';
import Button, { TypeButtonSize, TypeButtontype } from './reusable/Button';
import styles from '@styles/HomePage.module.scss';
import Card from './reusable/Card';
import ExtendedList from './reusable/ExtendedList';
import Footer from './reusable/Footer';

const extendedList_Array = [
  {
    id: 0,
    heading: 'Why do I need an NFT?',
    para: 'The TARA NFT acts as your login credentials. There is no need to create a username or password. Simply hold the NFT in your Ethereum based wallet and connect to the application. Once connected, the website will verify you have the TARA NFT and grant access.',
  },
  {
    id: 1,
    heading: "How many NFTs are there?",
    para: 'There are only 500 NFTs. No more NFTs will be minted. Meaning the maximum number of subscribers possible at any given moment is 500.',
  },
  {
    id: 2,
    heading: "What was the mint price of the NFT ?",
    para: 'Tara NFT sale happened in November 2021. Mint price was 0.2 ETH',
  },
  {
    id: 3,
    heading: 'What if I don’t like the dashboard?',
    para: 'If you hold the TARA NFT and no longer find value in the portal, go ahead and resell the NFT on the secondary market (ie - OpenSea).',
  },
  {
    id: 4,
    heading: 'What are future features to come?',
    para: 'Lot’s of features are yet to be introduced, a roadmap will soon be live.',
  },
];

const HomePage: FunctionComponent = (): JSX.Element => {
  return (
    <div className={styles.container}>
      {/* <img src="star1.svg" className={styles.homeStar} alt="star1" /> */}
      <Header />

      {/* ------------------mid container---------------------- */}

      <div className={styles.midSubContainer}>
        <div className={styles.midSubContainerChild1}>
          {/* <img src="star2.svg" className={styles.star2} alt="star2" /> */}
          <div className={styles.midSubContainerChild1_h1}>
            <h1>Exclusive Alpha</h1>
          </div>
          <div className={styles.midSubContainerChild1_p}>
            <p>
              The next era of metrics, tools, and services from Jarvis Labs begins here. Tara is the web3 dashboard giving TARA NFT holders access to a portal of reliable investment tools. For the first time ever, alpha now has limited access.
            </p>
          </div>
          <div className={styles.midSubContainerChild1_btn}>
            <a href='https://opensea.io/collection/tara-v2' target="_blank" rel="noopener noreferrer">
            <Button
              btnType={TypeButtontype.PRIMARY}
              btnSize={TypeButtonSize.MEDIUM}
              onClick={() => console.log('clicked')}
              label="Buy Now"
            />
            </a>
          </div>
        </div>
        <div className={styles.midSubContainerChild2}>
          <img src="star1.svg" className={styles.star1} alt="star1" />

          <img src="tara-dashboard-new.png" alt="dashboard tara" />
        </div>
      </div>
      <img 
        src="left_image.png"  
        className={styles.leftstar}
        alt="dashboard tara" 
      />
      <div className={styles.bottom_container}>
        <div className={styles.bottom_container_card}>
          <Card />
        </div>
        <div className={styles.List_Container}>
          {extendedList_Array.map((d) => (
            <ExtendedList
              key={d.id}
              heading={d.heading}
              value={d.id + 1}
              paraContent={d.para}
            />
          ))}
        </div>
      </div>
      <Footer />
    </div>
  );
};

//test

export default HomePage;
