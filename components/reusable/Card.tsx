import React, { FunctionComponent, memo } from 'react';
import styles from '@styles/Card.module.scss';
import Button, { TypeButtonSize, TypeButtontype } from './Button';

const Card: FunctionComponent = (): JSX.Element => {
  return (
    <div className={styles.card}>
      <div className={styles.card_header}>
        <h1>Where we push the boundaries of what is possible</h1>
      </div>
      <div className={styles.card_mid_section}>
        <p className={styles.para_1}>
        Tara is a testing ground for Jarvis Labs. It is where its hub of alpha hackers releases their latest creations for feedback by the community. 
        </p>
        <p className={styles.para_2}>
        The team leverages the largest onchain infrastructure and centralized exchange database to uncover trade signals. And by leaning on artificial intelligence the team of data scientists, engineers, researchers, and economists look to make Tara their testnet for tomorrow’s products.
        </p>
        <p className={styles.para_1}>
        Come join the community and experience what gated alpha looks like. 
        </p>
      </div>
      <div className={styles.card_bottom}>
        <a href='https://app.tara-nft.com' target="_blank" rel="noopener noreferrer">
        <Button
          btnSize={TypeButtonSize.MEDIUM}
          btnType={TypeButtontype.SECONDARY}
          label={'Go to Dashboard'} 
          style={{ border: 'none' }}
        />
        </a>   
      </div>
    </div>
  );
};

export default memo(Card);
